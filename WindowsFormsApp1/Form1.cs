﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Life;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        private Graphics graphics;
        private int resolution;
        private GameLife gameLife;




        public Form1()
        {
            InitializeComponent();
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }
        private void Start()
        {


            if (timer1.Enabled)
                return;
            NUD1.Enabled = false;
            NUD2.Enabled = false;

            resolution = (int)NUD1.Value;

            gameLife = new GameLife
                (
                rows: pictureBox1.Height / resolution,
                cols: pictureBox1.Width / resolution,
                density: (int)(NUD2.Minimum)+ (int)(NUD2.Maximum) - (int)(NUD2.Value)
                ) ;




            pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            graphics = Graphics.FromImage(pictureBox1.Image);
            timer1.Start();
        }
        private void DrawNextGeneration()
        {
            graphics.Clear(Color.Black);


            var field = gameLife.GetCurrentGeneration();
            for (int x = 0; x < field.GetLength(0); x++)
            {
                for (int y = 0; y < field.GetLength(1); y++)
                {
                    if (field[x, y])
                    {
                        if (checkBox1.Checked == true)
                        {

                            graphics.FillRectangle(Brushes.Red, x * resolution, y * resolution, resolution, resolution);
                            if (checkBox3.Checked == true)
                            {

                                graphics.FillRectangle(Brushes.Green, x * resolution, y * resolution, resolution, resolution);
                            }
                            else if (checkBox4.Checked == true)
                            {
                                graphics.FillRectangle(Brushes.Blue, x * resolution, y * resolution, resolution, resolution);

                            }
                        }
                        else if (checkBox2.Checked == true)
                        {
                            graphics.FillEllipse(Brushes.Red, x * resolution, y * resolution, resolution, resolution);

                            if (checkBox3.Checked == true)
                            {

                                graphics.FillEllipse(Brushes.Green, x * resolution, y * resolution, resolution, resolution);
                            }
                            else if (checkBox4.Checked == true)
                            {

                                graphics.FillEllipse(Brushes.Blue, x * resolution, y * resolution, resolution, resolution);
                            }



                        }
                    }
                }
            }
            pictureBox1.Refresh();
            gameLife.NextGeneration();
        }

        private void StopGame()
        {
            if (!timer1.Enabled)

                return;
            timer1.Stop();
            NUD1.Enabled = true;
            NUD2.Enabled = true;

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DrawNextGeneration();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Start();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            StopGame();
        }

        private void NUD2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!timer1.Enabled)          
                return;       
            
            if (e.Button == MouseButtons.Left)
            {
                var x = e.Location.X / resolution;
                var y = e.Location.Y / resolution;
                gameLife.AddCell(x, y);

            }
            if (e.Button == MouseButtons.Right)
            {
                var x = e.Location.X / resolution;
                var y = e.Location.Y / resolution;
                gameLife.RemCell(x, y);
            }

        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

    }
}